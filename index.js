console.log("Hello");

console.log("\n\nWhile Loop");
// While loop
/*
		- while loop takes in an expression/condition, if the condition evaluates to true, the statements inside the code block will be executed


	Syntax:

		while (expression/condition){
			statement/s;
		}

*/


/*let count = 5;

while(count >= 0){
	console.log("While: " + count);
	--count;
}
*/



/*
	Sample Output
	while: 5
	while: 4
	while: 3
	while: 2
	while: 1
	while: 0
*/



let count = 0;

while(count < 10){
	console.log("While: " + count);
	++count;
}





console.log("\n\nDo While Loop");
/*
	Do While Loop
		-	a do-while loop works a lot like the while loop. But unlike the while loop the do-while loops gaurantee that the code will be executed at least once.


	
	Syntax:

		do{
			statement/s;
		}while(condition)	
*/
 
/*let num = Number(prompt("Give me a number"));

do{
	console.log("Do While: " + num);
	num += 1;
}while(num < 10)*/




console.log("\n\nFor Loop");
// For loop
/*
		- for loop is more flexible than while do-while loops. It consists of three parts;

			1. Initialization - a value that will track the progression of the loop
			2. Condition - determines whether the loop will run one more time
			3. Final Expression - indicates how to advance the loop

	
	Syntax:

		for(initialization; condition; finalExpression){
			statement/s;
		}

*/


for(let i = 0; i <= 20; ++i){
	console.log(i);
}





console.log("\n\nLength of String");
// Cool things to know

let myString = "Geodor";

console.log(myString.length);


// you can do this to check whether number siya o hindi
console.log("number" === typeof myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);
console.log(myString[4]);
console.log(myString[5]);


console.log("\n\nUsing For Loop");

for(let i = 0; i < myString.length; ++i){
	console.log(myString[i]);
}



console.log("\n\nUsing For Loop and If Statements");


let myName = "AlEx";

for(let i = 0; i < myName.length; ++i){
	if(	
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}







console.log("\n\nContinue and Break");
/*
	Continue and Break

		Continue - a statement that allows teh code to go to the next iteration of the lopp without finishing the execution of all statements in a code block

		Break - a statement that is used to terminate the current loop once a match has been found


*/

for(let i = 0; i <= 20; ++i){

	if(i % 2 === 0){
		continue;
	}

	console.log("Continue and Break: " + i);

	if(i > 10){
		break;
	}
}



console.log("\n\nExperiment on Prime Numbers");

let prime = true;

for(let i = 0; i < 50; ++i){

	for(let j = 2; j < i; ++j){

		prime = 1;

		if(i % j === 0){
			prime = false;
			break;
		}
	}

	if(prime == true){
		console.log(i);
	}
}




console.log("\n\nIterating the length of a string");

let name = "alexandro";

for(let i = 0; i < name.length; ++i)
{
	console.log(name[i]);

	if(name[i].toLowerCase() === 'a')
	{
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] === 'd')
	{
		break;
	}
}